package com.avantaj.pg.camera;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.avantaj.pg.R;

public class UsingCameraAPIActivity extends Activity {

	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;

	public static final String RESULT_STATUS_SUCCESS = "success";
	public static final String RESULT_STATUS_ERROR_NO_MEMORY = "error_no_memory";
	public static final String RESULT_STATUS_ERROR = "error";

	private Camera mCamera = null;
	private CameraPreview mPreview;

	private Timer timer = null;
	private int ticks = -1;

	private int filecount = 0;

	private static final String TAG = "CAM2:";

	private JSONObject resultJSonObject = null;
	private JSONArray resultPathArray = null;

	private PictureCallback mPicture = new PictureCallback() {

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {

			File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE, filecount);
			Log.w(TAG, Uri.fromFile(pictureFile).toString());
			if (pictureFile == null) {
				Log.d(TAG,
						"Error creating media file, check storage permissions: ");
				return;
			}

			try {
				FileOutputStream fos = new FileOutputStream(pictureFile);
				fos.write(data);
				fos.close();
			} catch (FileNotFoundException e) {
				Log.d(TAG, "File not found: " + e.getMessage());
			} catch (IOException e) {
				Log.d(TAG, "Error accessing file: " + e.getMessage());
				sendResultAndExit(RESULT_STATUS_ERROR_NO_MEMORY);
			}

			mCamera.startPreview();

		}
	};

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main);

		filecount = 0;

		createResultJSONArray();

		/*
		 * // Add a listener to the Capture button Button captureButton =
		 * (Button) findViewById(R.id.button_capture);
		 * captureButton.setOnClickListener(new View.OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // get an image from the
		 * camera mCamera.takePicture(null, null, mPicture); } });
		 */

		Button exitButton = (Button) findViewById(R.id.button_exit);
		exitButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sendResultAndExit(RESULT_STATUS_SUCCESS);
			}
		});

		Button playPauseButton = (Button) findViewById(R.id.button_playppause);
		playPauseButton.setText("Pause");
		playPauseButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (ticks == -1) {
					((Button) v).setText("Pause");
					startTimer();
				} else {
					((Button) v).setText("Play");
					stopTimer();
				}
			}
		});
	}

	private void createResultJSONArray() {
		
		try {
//			JSONObject status = new JSONObject();

//			status.put("status", RESULT_STATUS_SUCCESS);

			resultJSonObject = new JSONObject();
			resultPathArray = new JSONArray();

			resultJSonObject.put("status", RESULT_STATUS_SUCCESS);

//			JSONObject jsonFolder = new JSONObject();
//			jsonFolder.put("folder", getIntent().getStringExtra("folder"));
			resultJSonObject.put("folder", getIntent().getStringExtra("folder"));

//			JSONObject jsonFilePrefix = new JSONObject();
//			jsonFilePrefix.put("fileprefix",getIntent().getStringExtra("fileprefix"));
			resultJSonObject.put("fileprefix",getIntent().getStringExtra("fileprefix"));

//			JSONObject jsonPathArray = new JSONObject();
//			jsonPathArray.put("pathArray", resultPathArray);
			resultJSonObject.put("pathArray", resultPathArray);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			sendResultAndExit(RESULT_STATUS_SUCCESS);
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	private void sendResultAndExit(String statusString) {

		try {
			resultJSonObject.put("status", statusString);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// get an image from the camera
		Intent intent = new Intent();
		intent.putExtra("result", resultJSonObject.toString());
		
		int status = RESULT_OK;
		if(!statusString.equals(RESULT_STATUS_SUCCESS)){
			status = RESULT_CANCELED;
		}
		setResult(status, intent);
		finish();
	}

	@Override
	protected void onResume() {

		super.onResume();
		// Create an instance of Camera
		mCamera = getCameraInstance();
		startTimer();
		// Create our Preview view and set it as the content of our activity.
		mPreview = new CameraPreview(this, mCamera);
		FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
		preview.removeAllViews();
		preview.addView(mPreview);
	};

	public void startTimer() {
		// Syncronous
		ticks = 0;
		timer = new Timer();
		long intravel = getIntent().getIntExtra("intravel", 3) * 1000;

		timer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				Log.w(">>> TICKS = ", "" + ticks);
				if (ticks == -1) {
					Log.w(">>> TICKS", "RETURNING");
					return;
				}
				filecount++;
				ticks++;
				mCamera.takePicture(null, null, mPicture);

			}
		}, intravel, intravel);
	}

	public void stopTimer() {
		ticks = -1;
		timer.cancel();
	}

	public boolean checkCameraHardware(Context context) {

		return context.getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA);

	}

	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open();
		} catch (Exception e) {
			Log.w("CAM:", "Camera not present", e);
		}
		return c;

	}

	@Override
	protected void onPause() {
		super.onPause();
		// releaseMediaRecorder(); // if you are using MediaRecorder, release it
		// first
		stopTimer();
		releaseCamera(); // release the camera immediately on pause event
	}

	private void releaseCamera() {

		Log.w(">>>> RELEASE", "RELEASE");
		if (mCamera != null) {
			mCamera.release(); // release the camera for other applications
			mCamera = null;
		}
	}

	/** Create a File for saving an image or video */
	private File getOutputMediaFile(int type, int count) {
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.
		String folderName = getIntent().getStringExtra("folder");
		String filePrefix = getIntent().getStringExtra("fileprefix");

		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				folderName);
		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(folderName, "failed to create directory");
				return null;
			}
		}

		// Create a media file name
		// String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
		// .format(new Date());

		Log.w("PHOTO AT ", mediaStorageDir.getPath() + File.separator
				+ filePrefix + "_" + count + ".jpg");

		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ filePrefix + "_" + count + ".jpg");
		} else if (type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ filePrefix + "_" + count + ".mp4");
		} else {
			return null;
		}
		resultPathArray.put(filePrefix + "_" + count);
		return mediaFile;
	}
}
